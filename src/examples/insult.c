#include <stdio.h>
#include <syscall.h>

int
main (int argc, char **argv)
{

  int w=wait(3);
  printf("Wait: %d\n", w);

  w=wait(4);
  printf("Wait: %d\n", w);

  w=wait(5);
  printf("Wait: %d\n", w);

  exit(0);
}
