#include <stdio.h>
#include <syscall.h>

int
main (int argc, char **argv)
{

  int i;

  for (i = 1; i < argc; i++)
  {
    int tid=exec(argv[i]);
    int w= wait(tid);
    printf ("%s tid: %d wait: %d\n", argv[i], tid, w);
  }

  exit(0);
}
